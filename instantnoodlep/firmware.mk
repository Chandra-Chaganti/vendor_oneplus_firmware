LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),instantnoodlep)

RADIO_FILES := $(wildcard $(LOCAL_PATH)/radio/*)
$(foreach f, $(notdir $(RADIO_FILES)), \
    $(call add-radio-file,radio/$(f)))

FIRMWARE_IMAGES := \
    abl \
    aop \
    bluetooth \
    cmnlib64 \
    cmnlib \
    devcfg \
    dsp \
    featenabler \
    hyp \
    imagefv \
    keymaster \
    logo \
    mdm_oem_stanvbk \
    modem \
    multiimgoem \
    qupfw \
    reserve \
    spunvm \
    storsec \
    tz \
    uefisecapp \
    xbl_config \
    xbl

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)

endif
